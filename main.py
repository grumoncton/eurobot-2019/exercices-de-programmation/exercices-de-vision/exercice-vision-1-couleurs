"""Code de vision pour des images de simulation"""

import os
import cv2
import numpy as np

# Couleurs
COLOURS = dict(red01=dict(minimum=np.array([170, 100, 100], np.uint8),
                          maximum=np.array([180, 255, 255], np.uint8)),
               red02=dict(minimum=np.array([0, 100, 100], np.uint8),
                          maximum=np.array([10, 255, 255], np.uint8)),
               green=dict(minimum=np.array([40, 100, 100], np.uint8),
                          maximum=np.array([60, 255, 255], np.uint8)),
               blue=dict(minimum=np.array([95, 100, 100], np.uint8),
                         maximum=np.array([115, 255, 255], np.uint8)))


def show_picture(img, window_name):
    """ Ouvre une fenêtre pour voir une image

    :param img: array numpy contenant l'image
    :param window_name: nom de la fenêtre pour l'image

    """
    # Ouvre une fenêtre
    cv2.imshow(window_name, img)
    # Attend qu'une touche de clavier soit appuyée
    cv2.waitKey(0)
    # Ferme toutes les fenêtres d'OpenCV
    cv2.destroyAllWindows()


def filter_colours(img_filename, img_dict):
    """Filtre différentes couleurs

    :img_filename: nom du fichier d'image
    :img_dict: dictionnaires contenant toutes les données

    """

    # Importe la photo
    img = cv2.imread(os.path.join('inp', img_filename))

    # Montre la photo traitée
    # show_picture(img, 'img')

    # Convertit l'image en HSV
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

    # Ajoute le HSV à un dictionnaire pour référence
    img_dict[img_filename]["hsv"] = hsv

    # On cherche chaque nom de couleur dans le dictionnaire COLOURS
    for color in COLOURS:

        # Ajout d'un dictionnaire à l'entrée img_dict[filename][color]
        img_dict[img_filename][color] = dict()

        # Récupération des couleurs dans le dictionnaire COLOURS
        lwr_hsv = COLOURS[color]["minimum"]
        upr_hsv = COLOURS[color]["maximum"]

        # Créé un masque entre lew_hsv et upr_hsv
        mask = cv2.inRange(hsv, lwr_hsv, upr_hsv)

        # Ajoute le masque au dictionnaire pour référence
        img_dict[img_filename][color]["mask"] = mask

        # Image finale
        res = cv2.bitwise_and(img, img, mask=mask)

        # Ajoute le résultat au dictionnaire pour référence
        img_dict[img_filename][color]["res"] = res

        # show_picture(mask, f'mask-{color}')
        # show_picture(res, f'res-{color}')

        out_filename = f"{img_filename[:-4]}-{color}-res.png"
        cv2.imwrite(os.path.join('out', out_filename), res)

    print(img.shape)


if __name__ == "__main__":

    # Création d'un dictionnaire comportant les données pour chaque image
    IMAGES = dict()

    for filename in os.listdir('inp'):
        IMAGES.setdefault(filename, dict())
        filter_colours(filename, IMAGES)
        # if filename == 'sim08.PNG':
        #     filter_colours(filename, IMAGES)
