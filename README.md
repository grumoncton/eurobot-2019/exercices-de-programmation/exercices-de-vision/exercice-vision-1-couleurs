# Vision - Couleurs

- [Vision - Couleurs](#vision---couleurs)
  - [Introduction à la couleur](#introduction-%C3%A0-la-couleur)
    - [RGB](#rgb)
    - [BGR](#bgr)
    - [HSV dans GIMP](#hsv-dans-gimp)
    - [**HSV dans OpenCV**](#hsv-dans-opencv)
  - [**Code**](#code)
    - [Couleurs dans le code](#couleurs-dans-le-code)

## Introduction à la couleur

En informatique, on décrit les couleurs en triplets de nombres.

### RGB

Ainsi, dans le système RGB, on décrit les couleurs comme une combinaison de trois couleurs primaires, à savoir le _rouge_, le _bleu_ et le _vert_. C'est d'ailleurs de ces couleurs que vient le nom _RGB_ :

| Nom en français | Nom en anglais | Lettre |
| --------------- | -------------- | :----: |
| Rouge           | Red            |   R    |
| Vert            | Green          |   G    |
| Bleu            | Blue           |   B    |

Ce système est souvent utilisé pour la sélection des couleurs.

![Interface de sélection de couleurs](https://upload.wikimedia.org/wikipedia/en/thumb/1/12/RGB_sliders.svg/1280px-RGB_sliders.svg.png)

Il est à noter que ces trois couleurs sont la base des couleurs sur votre écran. En effet, les écrans sont composées de petites lumières rouges, bleues et vertes, comme le montre cette image d'un écran vu au microscope :

![Lumières RGB d'un écran](https://upload.wikimedia.org/wikipedia/commons/1/1c/Light_pixel-beam.jpg)

Vous comprendrez donc qu'un écran jaune [n'est pas vraiment jaune](https://youtu.be/R3unPcJDbCc)

Mais qu'en est-t-il des nombres ? Il est dit plus haut que les couleurs sont des triplets de nombres, mais quelles valeurs peuvent être dans ces triplets ? Le tableau suivant donne les valeurs limites du système RGB.

| **Valeur** | **Min** | **Max** |
| ---------- | ------- | ------- |
| R          | 0       | 255     |
| G          | 0       | 255     |
| B          | 0       | 255     |

Ainsi, pour avoir les couleurs de base, il faut utiliser les triplets suivants. Pour une documentation plus complète sur le sujet, [suivre ce lien](https://rgb.to/0,0,0)

| **Couleur**              | **R** | **G** | **B** |
| ------------------------ | ----- | ----- | ----- |
| Noir                     | 0     | 0     | 0     |
| Blanc                    | 255   | 255   | 255   |
| **Couleurs primaires**   |
| Rouge                    | 255   | 0     | 0     |
| Vert                     | 0     | 255   | 0     |
| Bleu                     | 0     | 0     | 255   |
| **Couleurs secondaires** |
| Cyan                     | 0     | 255   | 255   |
| Magenta                  | 255   | 0     | 255   |
| Jaune                    | 255   | 255   | 0     |

### BGR

Pour faire de la vision, nous allons utiliser la librairie OpenCV. Or, cette librairie ne lit pas les images en RGB. En effet, elle lit les images en BGR, pour _Blue_, _Green_, _Red_.

Mis à part cela, les valeurs restent les mêmes que dans le système RGB.

<img align="right" height="200" src="https://upload.wikimedia.org/wikipedia/commons/4/45/The_GIMP_icon_-_gnome.svg" />

### HSV dans GIMP

Nous utiliserons plus souvent le système HSV que les systèmes RGB et BGR, surtout pour filtrer les couleurs.

En effet, ce système a comme avantage de faciliter la sélection des nuances d'une même couleur. Par exemple, si je veux chercher pour des teintes de rouge, je peux facilement spécifier un intervalle de nuances de rouge.

Concernant la théorie, _HSV_ signifie _Hue Saturation Value_.

| **Nom en français** | **Nom en anglais** | **Lettre** |
| ------------------- | ------------------ | ---------- |
| Teinte              | Hue                | H          |
| Saturation          | Saturation         | S          |
| Valeur              | Value              | V          |

La figure suivante montre comment sont disposées les valeurs du système HSV par rapport aux couleurs.

![Cône HSV](https://upload.wikimedia.org/wikipedia/commons/e/ea/HSV_cone.png)

Ainsi, si vous voulez du noir, il faut minimiser la valeur de _V_.
Pour le blanc, il faut minimiser la valeur de _S_ et maximiser la valeur de _V_.

Pour faciliter la sélection de la couleur, certaines personnes utilisent aussi un modèle triangulaire pour représenter l'espace HSV :

![Triangle HSV](https://upload.wikimedia.org/wikipedia/commons/1/1b/Triangulo_HSV.png)

On peut ainsi voir à quoi correspondent les valeurs de _H_, de _S_ et de _V_:

- Le H représente un angle sur le cercle externe (aussi appelé "cercle des couleurs")
- Le S représente l'_intensité_ ou la _pureté_ de la couleur
- Le V représente la _brillance_ de la couleur

Mais qu'en est-il des nombres, des valeurs ? Pouvons-nous définir une valeur de _V_ dans les millions ? Non, pas tellement. Voici les valeurs limites de l'espace HSV :

| **Valeur** | **Min** | **Max** |
| ---------- | ------- | ------- |
| H          | 0°      | 360°    |
| S          | 0 %     | 100 %   |
| V          | 0 %     | 100 %   |

<img align="right" height="200" src="https://upload.wikimedia.org/wikipedia/commons/5/53/OpenCV_Logo_with_text.png" />

### **HSV dans OpenCV**

Les limites de l'espace HSV dans OpenCV sont les suivantes :

| **Valeur** | **Min** | **Max** |
| ---------- | ------- | ------- |
| H          | 0°      | 180°    |
| S          | 0       | 255     |
| V          | 0       | 255     |

Ainsi, si vous voulez certaines couleurs dans OpenCV:

| **Couleur**              | **H**   | **S** | **V** |
| ------------------------ | ------- | ----- | ----- |
| Noir                     | x       | x     | 0     |
| Blanc                    | x       | 0     | 255   |
| **Couleurs primaires**   |
| Rouge                    | 0°/180° | 255   | 255   |
| Vert                     | 60°     | 255   | 255   |
| Bleu                     | 120°    | 255   | 255   |
| **Couleurs secondaires** |
| Cyan                     | 90°     | 255   | 255   |
| Magenta                  | 150°    | 255   | 255   |
| Jaune                    | 30°     | 255   | 255   |

Ou encore, vous pouvez vous référer sur ce _colormap_ :

![HSV Colormap](https://i.stack.imgur.com/gyuw4.png)

Enfin, voici quelques ressources :

- [Une question sur StackOverflow](https://stackoverflow.com/questions/10948589/choosing-the-correct-upper-and-lower-hsv-boundaries-for-color-detection-withcv)
- [Un tutoriel sur le filtrage avec OpenCV en C++](https://solarianprogrammer.com/2015/05/08/detect-red-circles-image-using-opencv/)
- [De la documentation d'OpenCV sur l'espace HSV](https://docs.opencv.org/3.4/df/d9d/tutorial_py_colorspaces.html)

## **Code**

Pour vous, j'ai programmé le code nécessaire pour l'algorithme dans le fichier [main.py](./main.py)

Ce code peut être utilisé comme référence.

Malgré tout, si vous voulez suivre l'atelier, exécutez les commandes ci-dessous

Si ce n'est pas déjà fait, cloner le répertoire avec les commandes suivantes
(dans git bash):

1. Créez un dossier pour le répertoire:
    ```sh
    mkdir -p ~/code/GRUM/eurobot2019/exercices/exercices-vision
    ```
1. Clonez le répertoire:
    ```sh
    git clone ssh://gitlab@gitlab.robitaille.host:49/GRUM/eurobot2019/exercices/exercice-vision-1-couleurs.git ~/code/GRUM/eurobot2019/exercices/exercices-vision/exercice-vision-1-couleurs
    ```
1. Ouvrez le dossier:
    ```sh
    cd ~/code/GRUM/eurobot2019/exercices/exercices-vision/exercice-vision-1-couleurs
    ```
1. Installez `virtualenv`
   ```sh
   pip3 install virtualenv
   ```
1. Créez un environnement virtuel et activez-le
   ```sh
   virtualenv venv
   source venv/Scripts/activate
   ```
1. Installez `numpy` et `opencv-python`
   ```sh
   pip3 install numpy opencv-python
   ```
1. Créez un nouveau fichier `atelier.py`. Vous pourrez y coder en direct.
    ```sh
    touch atelier.py
    ```
1. Ouvrez le dossier dans *Visual Studio Code*:
    ```sh
    code .
    ```

### Couleurs dans le code

Les couleurs des éléments du tableau furent trouvés à l'avance pour vous, dans un [fichier Excel](./couleurs/couleurs.csv).

<table border="1">
<thead><tr class="tableizer-firstrow"><th>Couleurs</th><th colspan=4 style="text-align:center">CMJN</th><th colspan=3 style="text-align:center">HSV - Gimp</th><th colspan=3 style="text-align:center">HSV - OpenCV</th><tbody>
 <tr><td>&nbsp;</td><td>C</td><td>M</td><td>J</td><td>N</td><td>H</td><td>S</td><td>V</td><td>H</td><td>S</td><td>V</td></tr>
 <tr><td>Équipe A</td><td>0%</td><td>25%</td><td>100%</td><td>0%</td><td>45</td><td>100</td><td>100</td><td>22,5</td><td>255</td><td>255</td></tr>
 <tr><td>Équipe B</td><td>50%</td><td>90%</td><td>0%</td><td>5%</td><td>266,67</td><td>90</td><td>95</td><td>133,34</td><td>229,5</td><td>242,25</td></tr>
 <tr><td>Bordures et éléments non colorés</td><td>15%</td><td>10%</td><td>25%</td><td>20%</td><td>80,00</td><td>16,67</td><td>72</td><td>40</td><td>42,51</td><td>183,6</td></tr>
 <tr><td>Redium</td><td>0%</td><td>100%</td><td>100%</td><td>10%</td><td>0,00</td><td>100</td><td>90</td><td>0</td><td>255</td><td>229,5</td></tr>
 <tr><td>Greenium</td><td>70%</td><td>0%</td><td>90%</td><td>0%</td><td>106,67</td><td>90</td><td>100</td><td>53,34</td><td>229,5</td><td>255</td></tr>
 <tr><td>Blueium</td><td>90%</td><td>40%</td><td>0%</td><td>0%</td><td>206,67</td><td>90</td><td>100</td><td>103,34</td><td>229,5</td><td>255</td></tr>
 <tr><td>Bordures Pente</td><td>100%</td><td>40%</td><td>50%</td><td>90%</td><td>170</td><td>100</td><td>6</td><td>85</td><td>255</td><td>15,3</td></tr>
</tbody></table>
